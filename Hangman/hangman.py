import random
import re

class answer:
    def __init__(self):
        self.category = random.choice(list(words.keys()))
        self.answer = words[self.category][random.randint(0,
        len(words[self.category])-1)]
        self.displayWord = ['_' for _ in range(len(self.answer))]

    def updateWord(self, char):
        for i in range(len(self.answer)):
            if char == self.answer[i]:
                self.displayWord[i] = char

class gameState:
    def __init__(self, maxAttempts):
        self.attempts = 0
        self.missedLetters = []
        self.answer = answer()
        self.maxAttempts = maxAttempts
        self.usedLetters = []
        self.winner = False

    def printBoard(self):
        print("HANGMAN")
        print("Category: " + self.answer.category)
        print(HANGMANPICS[self.attempts])
        print("Missed letters:", end=' ')
        print(*self.missedLetters, sep=', ')
        print("Secret word:", end=' ')
        print(*self.answer.displayWord, sep='')

    def updateState(self, input):
        if input in self.usedLetters:
            print("Already used that letter")
            return False
        elif input in self.answer.answer:
            self.answer.updateWord(input)
            self.usedLetters.append(input)
            self.printBoard()
            if not '_' in self.answer.displayWord:
                self.winner = True
                del self.answer
            return True
        else:
            self.usedLetters.append(input)
            self.missedLetters.append(input)
            self.attempts += 1
            self.printBoard()
            return True

    def reset(self):
        self.missedLetters = []
        self.answer = answer()
        self.usedLetters = []
        self.winner = False



HANGMANPICS = ['''
+---+
 |  |
    |
    |
    |
    |
=========''', '''
+---+
 |  |
 O  |
    |
    |
    |
=========''', '''
+---+
  |  |
  O  |
  |  |
     |
     |
=========''', '''
+---+
 |  |
 O  |
/|  |
    |
    |
=========''', '''
+---+
 |  |
 O  |
/|\ |
    |
    |
=========''', '''
+---+
 |  |
 O  |
/|\ |
/   |
    |
=========''', '''
+---+
 |  |
 O  |
/|\ |
/ \ |
    |
=========''']

words = {'Colors':'red orange yellow green blue indigo violet white black brown'.split(), 'Shapes':'square triangle rectangle circle ellipse rhombus trapezoid chevron pentagon hexagon septagon octagon'.split(), 'Fruits':'apple orange lemon lime pear watermelon grape grapefruit cherry banana cantaloupe mango strawberry tomato'.split(), 'Animals':'bat bear beaver cat cougar crab deer dog donkey duck eagle fish frog goat leech lion lizard monkey moose mouse otter owl panda python rabbit rat shark sheep skunk squid tiger turkey turtle weasel whale wolf wombat zebra'.split()}


while True:
    Hangman = gameState(6)
    Hangman.printBoard()
    while (Hangman.attempts < Hangman.maxAttempts or Hangman.winner == True):
        print("Take a guess")
        guess = input().lower()
        if re.match("[a-z]", guess) and len(guess) == 1:
            Hangman.updateState(guess)
        else:
            print("Wrong input")

    if Hangman.winner:
        print("You win!")
    else:
        print("You lose. Correct answer was ", end='')
        print(*Hangman.answer.answer,sep='')

    print("Do You want to play again?")
    done = input().lower()
    if not done.startswith('y'):
        break
    else:
        Hangman.reset()