#Chapter 14 Caesar's Cipher

MAX_KEY_SIZE = 26


shift = 0
while True:
    print("""Hello. What do You want to do?
    1. Encrypt
    2. Decrytpt""")
    tmp = input()
    if tmp == '1' or tmp.lower().startswith('e'):
        shift = 1
        break
    elif tmp == '2' or tmp.lower().startswith('d'):
        shift = -1
        break

message = ''
print("Enter Your message")
message = input().upper()

while True:
    print("Enter key number (1-26)")
    tmp = input()
    if tmp.isnumeric() and int(tmp) >=1 and int(tmp) <= MAX_KEY_SIZE:
        shift *= int(tmp)
        break

print("Your translated message is:")
translated = ''
for i in range(len(message)):
    ordInput = ord(message[i])
    if ordInput >64 and ordInput < 91:
        tmp = ordInput + shift
        if tmp > 90:
            tmp -= 26
        elif tmp < 65:
            tmp += 26
        translated += chr(tmp)
    else: 
        translated+= message[i]
print(translated)

