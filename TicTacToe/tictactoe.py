import random
import re

def insertMove(board, index, sign):
    if board[index] is not ' ':
        return False
    else:
        board[index] = sign
        return True

def checkWinner(board, sign):
    return ((board[0] == sign and board[1] == sign and board[2] == sign) or
        (board[3] == sign and board[4] == sign and board[5] == sign) or
        (board[6] == sign and board[7] == sign and board[8] == sign) or
        (board[0] == sign and board[4] == sign and board[8] == sign) or
        (board[2] == sign and board[4] == sign and board[6] == sign) or
        (board[0] == sign and board[3] == sign and board[6] == sign) or
        (board[1] == sign and board[4] == sign and board[7] == sign) or
        (board[2] == sign and board[5] == sign and board[8] == sign))

def checkTie(board):
    for _ in board:
        if _ == ' ':
            return False
    print("Tie!")
    return True

class AI:
    def __init__(self, sign):
        self.sign = sign

    def goCenter(self, emptyPlaces):
        if 5 in emptyPlaces:
            return 5
        else:
            return False

    def goCorner(self, emptyPlaces):
        corners = [0,2,6,8]
        while corners:
            pick = random.randint(0, len(corners)-1)
            candidate = corners.pop(pick)
            if candidate in emptyPlaces:
                return candidate
        return False

    def goSide(self, emptyPlaces):
        corners = [1,3,5,7]
        while corners:
            pick = random.randint(0, len(corners)-1)
            candidate = corners.pop(pick)
            if candidate in emptyPlaces:
                return candidate
        return False

    def copyBoard(self, board):
        dupeBoard = []
        for i in range(len(board)):
            dupeBoard.append(board[i])
        return dupeBoard

    def think(self, board):
        emptyPlaces = []
        for i in range(len(board)):
            if board[i] is ' ':
                emptyPlaces.append(i)
                copiedBoard = self.copyBoard(board)
                insertMove(copiedBoard, i, self.sign)
                if checkWinner(copiedBoard, self.sign):
                    return i

        for i in range(len(emptyPlaces)):
            copiedBoard = self.copyBoard(board)
            insertMove(copiedBoard, emptyPlaces[i], TicTacToe.playerSign)
            print(copiedBoard)
            if checkWinner(copiedBoard, TicTacToe.playerSign):
                return emptyPlaces[i]

        result = self.goCorner(emptyPlaces)
        if result:
            return result
        
        result = self.goCenter(emptyPlaces)
        if result:
            return result

        result = self.goSide(emptyPlaces)
        if result:
            return result

class gameState():
    def __init__(self):
        self.board = [' ' for i in range(9)]
        self.playerSign = ''
        self.AI = None
        self.playerTurn = False

    def pickSign(self):
        signs = ['X', 'O']
        while True:
            entry = input().upper()
            if entry in signs and len(entry) == 1:
                self.playerSign = entry
                index = signs.index(entry)
                self.AI = AI(signs[index - 1])
                break
            else:
                print("Wrong sign. Please enter again")

    def whoGoFirst(self):
        if (random.randint(0, 1) == 1):
            self.playerTurn = True


    def playerInput(self, index, sign):
        if self.board[index] is not ' ':
            print("Invalid move. Try again")
            return False
        else:
            self.board[index] = sign
            self.playerTurn ^= self.playerTurn
            return True

    def printBoard(self):
        print('   |   |   ')
        print(' ' + self.board[6] + ' | ' + self.board[7] + ' | ' + self.board[8])
        print('   |   |   ')
        print('-----------')
        print('   |   |   ')
        print(' ' + self.board[3] + ' | ' + self.board[4] + ' | ' + self.board[5])
        print('   |   |   ')
        print('-----------')
        print('   |   |   ')
        print(' ' + self.board[0] + ' | ' + self.board[1] + ' | ' + self.board[2])
        print('   |   |   ')

    def reset(self):
        self.__init__()

while True:
    print("Tic Tac Toe game!\nPick Your sign from X and O")
    TicTacToe = gameState()
    TicTacToe.pickSign()
    TicTacToe.whoGoFirst()
    while True:
        if TicTacToe.playerTurn == True:
            TicTacToe.printBoard()
            print("Your move")
            while True:
                tmp = input()
                if re.match("[1-9]", tmp) and len(tmp) == 1:
                    TicTacToe.playerInput(int(tmp)-1, TicTacToe.playerSign)
                    break
                else:
                    print("Wrong move. Try again")
            if checkWinner(TicTacToe.board, TicTacToe.playerSign):
                print("You win")
                break
        else:
            move = TicTacToe.AI.think(TicTacToe.board)
            TicTacToe.playerInput(move, TicTacToe.AI.sign)
            if checkWinner(TicTacToe.board, TicTacToe.AI.sign):
                print("You Lose")
                break
            TicTacToe.playerTurn ^= 1
        if checkTie(TicTacToe.board):
            break
    print("Do You want to play again?")
    answer = input().lower()
    if answer.startswith('y'):
        TicTacToe.reset()
    else:
        break
