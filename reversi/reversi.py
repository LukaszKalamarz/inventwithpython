#Chapter 15 Reversi
import re
import random
import sys

class gameState:
    def __init__(self):
        self.board = None
        self.playerScore = None
        self.computerScore = None
        self.playerSign = None
        self.computerSign = None
        self.playerMove = None

    def generateBoard(self):
        self.board = []
        for _ in range(8):
            self.board.append([' '] * 8)

        self.board[3][3] = 'X'
        self.board[3][4] = 'O'
        self.board[4][3] = 'O'
        self.board[4][4] = 'X'
        self.playerScore = 2
        self.computerScore = 2

    def printScore(self):
        print("Player score: %s Computer score: %s" %(self.playerScore, self.computerScore))

    def printBoard(self):
        print(" 12345678")
        i = 1
        while i < 9:
            print("%s" %i, *self.board[i-1],sep='')
            i += 1
        self.printScore()

    def chooseSign(self):
        while True:
            print("Choose Your sign (X O)")
            tmp = input().upper()
            if tmp == 'X':
                self.playerSign = 'X'
                self.computerSign = 'O'
                break
            elif tmp == 'O':
                self.playerSign = 'O'
                self.computerSign = 'X'
                break

    def whoGoFirst(self):
        choice = random.randint(0,1)
        if choice == 0:
            self.playerMove = True
        else:
            self.playerMove = False

    def gameInit(self):
        self.generateBoard()
        self.chooseSign()
        self.whoGoFirst()

    def declareWinner(self):
        if self.playerScore > self.computerScore:
            print("You win!")
        elif self.playerScore < self.computerScore:
            print("You lose")
        else:
            print("It's a tie!")

    def playAgain(self):
        print("Do You want to play again?")
        response = input().lower()
        if response.startswith('y'):
            return True
        else:
            return False

    def gameReset(self):
        self.__init__()

def isOnBoard(x,y):
    return x >=0 and x<= 7 and y>=0 and y<=7

def haveTile(board, x, y):
    if board[x][y] != ' ':
        return True
    else:
        return False

def isValidMove(board, x, y, sign):
    if sign == 'X':
        otherTile = 'O'
    else:
        otherTile = 'X'

    if not isOnBoard(x,y) or haveTile(board,x,y):
        return False

    tilesToFlip = []
    directions = [[0,1], [1,1], [1,0], [0,-1], [-1,-1], [-1,0], [1,-1], [-1,1]]
    #Brute force - book solution
    for xDirection, yDicrection in directions:
        xTmp, yTmp = x, y
        xTmp += xDirection
        yTmp += yDicrection
        if isOnBoard(xTmp, yTmp) and board[xTmp][yTmp] == otherTile:
            xTmp += xDirection
            yTmp += yDicrection

            if not isOnBoard(xTmp, yTmp):
                continue
            while board[xTmp][yTmp] == otherTile:
                xTmp += xDirection
                yTmp += yDicrection
                if not isOnBoard(xTmp, yTmp):
                    break
            if not isOnBoard(xTmp, yTmp):
                    continue
            if board[xTmp][yTmp] == sign:
                while True:
                    xTmp -= xDirection
                    yTmp -= yDicrection
                    if xTmp == x and yTmp == y:
                        break
                    tilesToFlip.append([xTmp, yTmp])
    if len(tilesToFlip) == 0:
        return False
    return tilesToFlip

def copyBoard():
    dupeBoard = []
    for i in range(len(reversi.board)):
        dupeBoard.append(reversi.board[i])
    return dupeBoard

def goCorner():
    pass

def getValidMoves(board, sign):
    validMoves = []
    for x in range(8):
        for y in range(8):
            if isValidMove(board, x, y, sign):
                validMoves.append([x, y])
    return validMoves

def computerMove():
    bestCoords = []
    possibleflips = []
    possibleMoves = getValidMoves(reversi.board, reversi.computerSign)
    maxGain = 0
    for i in range(len(possibleMoves)):
        x,y = possibleMoves[i]
        dupeBoard = copyBoard()
        flippedTiles = isValidMove(dupeBoard, x, y, reversi.computerSign)
        if flippedTiles:
            if len(flippedTiles) > maxGain:
                bestCoords = [x,y]
                maxGain = len(flippedTiles)
                possibleflips = flippedTiles

    x,y = bestCoords
    reversi.board[x][y] = reversi.computerSign
    reversi.computerScore +=1
    for i in range(len(possibleflips)):
        x,y = possibleflips[i]
        reversi.board[x][y] = reversi.computerSign
        reversi.computerScore +=1
        reversi.playerScore -=1

def playerMove():
    while True:
        print("Make move (x y) or (hint):")
        playerInput = input().split(' ')
        pattern = "[1-8]"
        if len(playerInput) == 2 and re.match(pattern, playerInput[0]) and re.match(pattern, playerInput[1]):
            x = int(playerInput[0]) - 1
            y = int(playerInput[1]) - 1
            y, x = x, y
            tilesToFlip = isValidMove(reversi.board, x, y, reversi.playerSign)
            if tilesToFlip:
                reversi.board[x][y] = reversi.playerSign
                reversi.playerScore += 1
                for i in range(len(tilesToFlip)):
                    x,y = tilesToFlip[i]
                    reversi.board[x][y] = reversi.playerSign
                    reversi.playerScore +=1
                    reversi.computerScore -= 1
                break
            else:
                print("Invalid move")
        elif playerInput[0] == 'hint':
            hint = ''
            for coords in getValidMoves(reversi.board, reversi.playerSign):
                hint += ("[%s %s], " %(coords[1] + 1, coords[0] + 1))
            print(hint)



reversi = gameState()
while True:
    reversi.gameReset()
    reversi.gameInit()
    while True:
        reversi.printBoard()
        if reversi.playerMove: 
            playerMove()
            if len(getValidMoves(reversi.board, reversi.computerSign)) == 0:
                break
        else:
            computerMove()
            if len(getValidMoves(reversi.board, reversi.playerSign)) == 0:
                break
        reversi.playerMove ^= 1
    reversi.printScore()
    reversi.declareWinner()
    if not reversi.playAgain():
        break
