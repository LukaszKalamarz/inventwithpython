import random

MAX_TREASURES = 3
MAX_SONARS = 16

board = []
for i in range(15):
    tmp = ['.' for _ in range(60)]
    board.append(tmp)

chestsLocations = []
def createChests():
    while len(chestsLocations) < MAX_TREASURES:
        candidate = [random.randint(0,59), random.randint(0,15)]
        if candidate not in chestsLocations:
            chestsLocations.append(candidate)

def validMove(x,y):
    if board[int(y)][int(x)] == '.':
        return True
    else:
        print("Coords already checked")
        return False

def playerMove():
    print("enter move")
    while True:
        move = input()
        move = move.split()
        if len(move) == 2:
            x, y = move[0], move[1]
            if x.isdigit() and y.isdigit() and int(x) in range(0,60) and int(y) in range(0,15) and validMove(x, y):
                return [int(x), int(y)]
        print("Enter coords in range [0-59] [0-15]")

def locateChest(coords):
    x,y = coords[0], coords[1]
    shortestDistance = 100
    distance = 0
    for cx, cy in chestsLocations:
        distance = max(abs(cx-x), abs(cy-y))
        if distance < shortestDistance:
            shortestDistance = distance
    
    if shortestDistance == 0:
        print("Treasure found!")
        board[y][x] = "T"
        chestsLocations.remove([x, y])
    elif shortestDistance< 10:
        print("Closest trasure in distance %s of sonar drop" %shortestDistance)
        board[y][x] = str(shortestDistance)
    else:
        board[y][x] = 'X'
        print("Sonar did not found anything")



def dropSonar():
    return playerMove()


moves = 1
createChests()
while moves < MAX_SONARS or len(chestsLocations) == 0:
    print("You still have %s sonar(s) left" %(MAX_SONARS-moves) )
    coords = dropSonar()
    locateChest(coords)
    print(*board, sep="\n")
    moves +=1
if len(chestsLocations)> 0:
    for x,y in chestsLocations:
        print("Missed location was %s %s" %(x, y))
else:
    print("All treasures found")