#Chapter 11. Bagels
import random

class gameState:

    def __init__(self, wordLength, max_guess):
        self.wordLength = wordLength
        self.max_guess = max_guess
        self.guesses = 1
        self.answer = ''
        self.playerEntry = ''
    
    def pickAnswer(self):
        number = list(range(10))
        random.shuffle(number)
        for i in range(self.wordLength):
            self.answer += str(number[i])

    def printIntro(self):
        print("""Intro part 
        Here are some clues:
        When I say: That means:
        Pico: One digit is correct but in the wrong position.
        Fermi: One digit is correct and in the right position. 
        Bagels: No digit is correct. 
        I have thought up a number. You have %s guesses to get it.""" %self.max_guess)
        print("I've chosen number. Start guessing")

    def gameSetup(self):
        self.pickAnswer()
        self.printIntro()

    def playAgain(self):
        print("Do You wanna play again?")
        return input().lower().startswith('y')

    def playerInput(self):
        playerInput = ''
        while len(playerInput) < self.wordLength:
            print("Enter Your guess #", str(self.guesses))
            tmp=input()
            if tmp.isnumeric():
                playerInput+=tmp
        self.playerEntry = playerInput

    def giveClues(self, playerInput):
        clues = []
        for i in range(self.wordLength):
            if playerInput[i] == self.answer[i]:
                clues.append('Fermi')
            elif playerInput[i] in self.answer:
                clues.append('Pico')
        if not clues:
            clues.append('Bagels')
        self.guesses += 1
        return clues

    def checkWin(self, playerInput):
        if playerInput == self.answer:
            return True

    def reset(self):
        self.__init__(self.wordLength, self.max_guess)
        self.gameSetup()


Bagels = gameState(3, 10)
while True:
    Bagels.reset()
    while (Bagels.guesses < Bagels.max_guess):
        Bagels.playerInput()
        if Bagels.checkWin(Bagels.playerEntry):
            print("You win")
            break
        else:
            print(*Bagels.giveClues(Bagels.playerEntry), sep=', ')
    if not Bagels.checkWin(Bagels.playerEntry):
        print("You lose. Correct number was %s" %Bagels.answer)
    if not Bagels.playAgain():
        break
